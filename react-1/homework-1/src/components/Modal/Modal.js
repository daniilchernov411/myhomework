import React, { Component } from "react";
import "./Modal.scss";

class Modal extends Component {
    render() {
        const { closeModal, header, closeButton, text, actions } = this.props;
        return (
            <div className="modal_overlay" onClick={closeModal}>
                <div className="modal">
                    <div className="modal_header">
                        <h2>{header}</h2>
                        {closeButton && <button className={"modal_close_btn"} onClick={closeModal} />}
                    </div>
                    <div className="modal_body">
                        <div className="modal_text">{text}</div>
                        <div>{actions}</div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal;
