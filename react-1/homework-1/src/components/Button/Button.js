import React from "react";
import "./Button.scss";

class Button extends React.Component {
    render() {
        const { onClick, backgroundColor, someText, id } = this.props;
        return (
            <button className="button" id={id} onClick={onClick} style={{ backgroundColor: backgroundColor }}>{someText}</button>
        );
    }
}

export default Button;
