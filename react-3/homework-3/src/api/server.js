import { useEffect, useState } from 'react';

const useServer = () => {
    const [clothes, setClothes] = useState([]);
    useEffect(() => {
        fetch('cards.json')
            .then(response => response.json())
            .then(data => setClothes(data))
            ;
    }, [])
    return clothes
}

export default useServer;