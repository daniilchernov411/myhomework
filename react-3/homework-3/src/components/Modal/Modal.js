import React from "react";
import "./Modal.scss";

const Modal = (props) => {
    const { onClick, header, closeButton, text, actions } = props;
    return (
        <div className="modal_overlay" onClick={onClick}>
            <div className="modal" onClick={e => e.stopPropagation()}>
                <div className="modal_header">
                    <h2>{header}</h2>
                    {closeButton && <button className={"modal_close_btn"} onClick={onClick} />}
                </div>
                <div className="modal_body">
                    <div className="modal_text">{text}</div>
                    <div>{actions}</div>
                </div>
            </div>
        </div>
    )
}

export default Modal;
