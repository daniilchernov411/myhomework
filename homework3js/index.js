'use strict';
let num1, num2, znak;
do {
    num1 = prompt('vvedite chislo 1');
} while (isNaN(+num1) || !num1 || num1.trim() === '');
do {
    num2 = prompt('vvedite chislo 2');
} while (isNaN(+num2) || !num2 || num2.trim() === '');
do {
    znak = prompt('vvedite operaciy');
} while (znak !== '/' && znak !== '*' && znak !== '+' && znak !== '-');

function operatin(num1, num2, znak) {
    if (znak === '/') {
        return(console.log(num1 / num2));
    } else if (znak === '*') {
        return(console.log(num1 * num2));
    } else if (znak === '-') {
        return(console.log(num1 - num2));
    } else if (znak === '+') {
        return(console.log(+num1 + +num2));
    }
}
operatin(num1, num2, znak);