import React, { useEffect } from 'react'
import CardList from '../components/CardList/CardList'
import fetchCards from '../store/card/actions'
import { useSelector, useDispatch } from 'react-redux'


const Home = () => {
    const dispatch = useDispatch()
    const phones = useSelector(store => store.cardReducer.phones)


    useEffect(() => {
        dispatch(fetchCards())
    }, [dispatch])
    return (
        <CardList phonesList={phones} />
    )
}


export default Home;