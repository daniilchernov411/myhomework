import React, { useState, useEffect } from 'react'
import AllCard from '../components/AllCards/AllCard'
import fetchCards from '../store/card/actions'
import { useSelector, useDispatch } from 'react-redux'


const Favorites = () => {
    const dispatch = useDispatch()
    const phones = useSelector(store => store.cardReducer.phones)
    useEffect(() => {
        dispatch(fetchCards())
    }, [dispatch])

    const [favsphones, setFavsphones] = useState([])
    const articleArr = localStorage.getItem('favorite');
    const parsedArr = JSON.parse(articleArr)
    useEffect(() => {
        if (parsedArr) {
            setFavsphones(
                phones.filter(obj => parsedArr.find(id => id === obj.article))
            )
        } else {
            setFavsphones(false)
        }
    }, [phones, parsedArr])
    return (
        <div>
            <h1>Favorites :</h1>
            {favsphones && <AllCard phonesList={favsphones} />}
        </div>
    )
}

export default Favorites;