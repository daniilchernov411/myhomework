import React, { useState } from 'react';
import Modal from "../../components/Modal/Modal";
import Card from '../Card/Card';
import "./AllCard.scss";
import { useSelector, useDispatch } from 'react-redux'
import { openModal, clearModal } from "../../store/modal/actions"

const AllCard = (props) => {
    const modalIsOpen = useSelector(store => store.modalReducer.modalIsOpen)
    const dispatch = useDispatch()
    const [prodArticle, setprodArticle] = useState(null);
    const [favPhones, setFavPhones] = useState(false);
    const addFavorite = (id) => {
        const arrayFavorite = localStorage.getItem('favorite');
        if (arrayFavorite) {
            const arrayParse = JSON.parse(arrayFavorite);
            const indexFavorite = arrayParse.indexOf(id);
            if (indexFavorite === -1) {
                arrayParse.push(id);
                localStorage.setItem('favorite', JSON.stringify(arrayParse))
                setFavPhones(true);
            } else {
                arrayParse.splice(indexFavorite, 1)
                localStorage.setItem('favorite', JSON.stringify(arrayParse))
                setFavPhones(true);
            }
        } else {
            localStorage.setItem('favorite', JSON.stringify([id]))
        }
    }

    const showModal = () => {
        dispatch(openModal())
    }

    const closeModal = () => {
        if (modalIsOpen) {
            dispatch(clearModal())
        }
    }

    const addId = (id) => {
        setprodArticle(id)
        showModal()
    }

    const saveCart = (id) => {
        const arrayCards = localStorage.getItem('card');
        if (modalIsOpen) {
            dispatch(clearModal())
        }
        if (arrayCards) {
            const arrayParse = JSON.parse(arrayCards);
            const indexCard = arrayParse.indexOf(id);
            if (indexCard === -1) {
                arrayParse.push(id)
                localStorage.setItem('card', JSON.stringify(arrayParse))
            }
        } else {
            localStorage.setItem('card', JSON.stringify([id]))
        }
    }
    const removeFromCart = (id) => {
        const arrayCards = localStorage.getItem('card')
        if (modalIsOpen) {
            dispatch(clearModal())
        }
        if (arrayCards) {
            const arrayParse = JSON.parse(arrayCards)
            const indexCard = arrayParse.indexOf(id)
            arrayParse.splice(indexCard, 1)
            localStorage.setItem('card', JSON.stringify(arrayParse))
        }
    }
    const arrCards = props.phonesList.map((item) => {
        return (
            <Card
                key={item.article}
                name={item.name}
                article={item.article}
                image={item.imageUrl}
                price={item.price}
                productColor={item.color}
                favPhones={favPhones}
                isCart={props.isCart}
                onClickStar={() => addFavorite(item.article)}
                onClickAddToCart={() => addId(item.article)}
                onClickSaveToCart={() => saveCart(item.article)} />
        )
    })
    return (
        <>
            <ul className={'all_cards'}>
                {arrCards}
            </ul>
            {modalIsOpen &&
                <Modal
                    onClick={closeModal}
                    header={"Add to basket"}
                    text={"Do you wont add this phone to the basket?"}
                    closeButton={true}
                    actions={
                        <div>
                            <button className={"modal_buttons"} onClick={() => {
                                props.isCart ? removeFromCart(prodArticle) :
                                    saveCart(prodArticle)
                            }}>Ok</button>
                            <button className={"modal_buttons"} onClick={closeModal}>Cancel</button>
                        </div>
                    }
                />
            }
            {(modalIsOpen && props.isCart) &&
                <Modal
                    onClick={closeModal}
                    header={"Remove to basket"}
                    text={"Do you wont remove this phone from the basket?"}
                    closeButton={true}
                    actions={
                        <div>
                            <button className={"modal_buttons"} onClick={() => {
                                props.isCart ? removeFromCart(prodArticle) :
                                    saveCart(prodArticle)
                            }}>Ok</button>
                            <button className={"modal_buttons"} onClick={closeModal}>Cancel</button>
                        </div>
                    }
                />
            }
        </>
    )
}


export default AllCard;