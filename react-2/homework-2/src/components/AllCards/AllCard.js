import React from 'react';
import Modal from "../../components/Modal/Modal";
import Card from '../Card/Card';
import "./AllCard.scss";

class AllCard extends React.Component {
    state = {
        openModal: false,
        prodArticle: null
    }

    addFavorite = (id) => {
        const arrayFavorite = localStorage.getItem('favorite');
        if (arrayFavorite) {
            const arrayParse = JSON.parse(arrayFavorite);
            const indexFavorite = arrayParse.indexOf(id);
            if (indexFavorite === -1) {
                arrayParse.push(id);
                localStorage.setItem('favorite', JSON.stringify(arrayParse))
            } else {
                arrayParse.splice(indexFavorite, 1)
                localStorage.setItem('favorite', JSON.stringify(arrayParse))
            }
        } else {
            localStorage.setItem('favorite', JSON.stringify([id]))
        }
    }

    showModal = () => {
        this.setState({ openModal: true })
    }

    closeModal = () => {
        this.setState({ openModal: false })
    }

    addId = (id) => {
        this.setState({ prodArticle: id })
        this.showModal()
    }

    saveCart = (id) => {
        const arrayCards = localStorage.getItem('card');
        this.setState({ openModal: false })
        if (arrayCards) {
            const arrayParse = JSON.parse(arrayCards);
            const indexCard = arrayParse.indexOf(id);
            if (indexCard === -1) {
                arrayParse.push(id)
                localStorage.setItem('card', JSON.stringify(arrayParse))
            }
        } else {
            localStorage.setItem('card', JSON.stringify([id]))
        }
    }

    render() {
        const arrCards = this.props.clothesList.map((item) => {
            return (
                <Card
                    key={item.article}
                    name={item.name}
                    article={item.article}
                    image={item.imageUrl}
                    price={item.price}
                    productColor={item.color}
                    onClickStar={() => this.addFavorite(item.article)}
                    onClickAddToCart={() => this.addId(item.article)}
                    onClickSaveToCart={() => this.saveCart(item.article)} />
            )
        })
        return (
            <>
                <ul className={'all_cards'}>
                    {arrCards}
                </ul>
                {this.state.openModal &&
                    <Modal
                        onClick={this.closeModal}
                        header={"Add to basket"}
                        text={"Do you wont add this phone to the basket?"}
                        closeButton={true}
                        actions={
                            <div>
                                <button className={"modal_buttons"} onClick={() => { this.saveCart(this.state.prodArticle) }}>Ok</button>
                                <button className={"modal_buttons"} onClick={this.closeModal}>Cancel</button>
                            </div>
                        }
                    />
                }
            </>
        )
    }
}

export default AllCard;