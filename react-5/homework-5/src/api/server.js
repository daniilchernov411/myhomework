import { useEffect, useState } from 'react';

const useServer = () => {
    const [phones, setPhones] = useState([]);
    useEffect(() => {
        fetch('cards.json')
            .then(response => response.json())
            .then(data => setPhones(data))
            ;
    }, [])
    return phones
}

export default useServer;