import React from "react";
import "./Button.scss";

const Button = (props) => {

    const { onClick, backgroundColor, someText, id } = props;
    return (
        <button className="button" id={id} onClick={onClick} style={{ backgroundColor: backgroundColor }}>{someText}</button>
    );
}

export default Button;
