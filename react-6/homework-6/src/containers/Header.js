import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
    return (
        <div className="menu">
            <Link to="/" className="menu-item">Home</Link>
            <Link to="/cart" className="menu-item">Cart</Link>
            <Link to="/favorites" className="menu-item">Favorites</Link>
        </div>
    )
}

export default Header;