import React from 'react';
import { useDispatch } from 'react-redux';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import Validate from './FormValidation';
import { setFormData, clearLocalStorage } from '../../store/form/actions'
import "./Form.scss";
import '../Button/Button.scss'
const CartForm = () => {
    const dispatch = useDispatch();
    return (
        <div>
            <h2 className={'form__header'}>Form</h2>
            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    age: '',
                    address: '',
                    phoneNumber: '',
                }}
                validationSchema={Validate}
                onSubmit={(values) => {
                    dispatch(setFormData(values))
                    dispatch(clearLocalStorage())
                    window.location.reload()
                }}
            >
                {({ handleSubmit }) => (
                    <Form onSubmit={handleSubmit} className={'form'}>
                        <div className={'form__allInputs'}>
                            <Field type='text' name='firstName' placeholder='First Name' className={'form__input'} />
                            <ErrorMessage name='firstName' />

                            <Field type='text' name='lastName' placeholder='Last Name' className={'form__input'} />
                            <ErrorMessage name='lastName' />

                            <Field type='numbet' name='age' placeholder='Age' className={'form__input'} />
                            <ErrorMessage name='age' />

                            <Field type='text' name='address' placeholder='Address' className={'form__input'} />
                            <ErrorMessage name='address' />

                            <Field type='text' name='phoneNumber' placeholder='Phone Number' className={'form__input'} />
                            <ErrorMessage name='phoneNumber' />
                        </div>
                        <button className={'button'} style={{ backgroundColor: 'lightblue' }} type='submit'>Buy</button>
                    </Form>
                )}
            </Formik>
        </div>
    )
}

export default CartForm;