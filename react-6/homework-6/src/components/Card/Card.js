import React, { useEffect, useState } from 'react';
import "./Card.scss";
import Button from '../Button/Button'

const Card = (props) => {

    const [colorStar, setColorStar] = useState('red');

    useEffect(() => {
        const arrItem = localStorage.getItem('favorite');
        const arrParce = JSON.parse(arrItem);
        if (arrParce) {
            arrParce.forEach(e => {
                if (e === props.article) {
                    setColorStar('green')
                }
            })
        }
    }, [])

    const drawColor = () => {
        if (colorStar === 'red') {
            setColorStar('green')
        } else if (colorStar === 'green') {
            setColorStar('red')
        }
        props.onClickStar()
    }

    const { article, name, image, price, productColor, onClickAddToCart } = props
    return (
        <>
            <li key={article} className="card">
                <img src={image} className={'card_img'} alt='clothes' />
                <div>
                    <div>
                        <p > Name: {name} </p>
                        <p > Price: {price} </p>
                        <p > Color: {productColor} </p>
                        <p > Acticle: {article} </p>
                    </div>
                    <div>
                        {
                            <div onClick={drawColor} >
                                <svg style={{ width: '20px', height: '20px' }}>
                                    <path style={{ fill: colorStar }} d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                </svg>
                            </div>
                        }
                        <div>
                            <Button someText={!props.isCart ? "Add to Basket" : "Remove from Basket"} onClick={onClickAddToCart} backgroundColor={'lightgreen'} />
                        </div>
                    </div>
                </div>

            </li>
        </>
    )

}

export default Card;